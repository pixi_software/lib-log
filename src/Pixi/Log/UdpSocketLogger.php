<?php

namespace Pixi\Log;

use Pixi\Log\LogLevel;

class UdpSocketLogger extends AbstractLogger
{
    /**
     * @var array
     */
    private static $server = [];

    /**
     * @var array
     */
    private static $params = [];

    /**
     * Init
     *
     * @param array $params
     * @return UdpSocketLogger
     */
    public static function init($params = [])
    {
        if (!isset($params['host']) || !isset($params['port'])) {
            throw new \InvalidArgumentException("UDP logger server not defined.");
        }

        $logger = self::getLogger();
        $logger::setServer($params['host'], $params['port']);

        return $logger;
    }

    /**
     * Configure UDP server
     *
     * @param string $host server host
     * @param int $port server port
     */
    public static function setServer ($host, $port)
    {
        self::$server['host'] = $host;
        self::$server['port'] = $port;
    }

    /**
     * Configure UDP logger
     *
     * @param int $env environment
     * @param string $cid customer ID
     * @param string $aid application ID
     * @param string $ver application version
     * @param string $usr application user
     */
    public static function setConfig($env = 0, $cid = null, $aid = null, $ver = null, $usr = null)
    {
        self::$params = [
            'env' => $env,
            'cid' => $cid,
            'aid' => $aid,
            'ver' => $ver,
            'usr' => $usr,
        ];
    }

    /**
     * Log entry
     *
     * @param string $level debug, info, notice, warning, error, critical, alert, emergency
     * @param string $message message
     * @param array $context message details
     * @param string $method method name
     * @param string $class class name
     * @param boolean $jsonContext encode context by default
     */
    public function log($level, $message, $details = array(), $method = null, $class = null, $jsonDetails = true)
    {
        if(LogLevel::$logLevel[$level] >= self::$logLevel) {
            $params = [
                'cid'     => self::$params['cid'],
                'src'     => 'web.apps',
                'aid'     => self::$params['aid'],
                'ver'     => self::$params['ver'],
                'env'     => self::$params['env'],
                'lvl'     => $level,
                'usr'     => self::$params['usr'],
                'class'   => $class,
                'method'  => $method,
                'msg'     => $message,
                'details' => $jsonDetails ? json_encode($details) : $details,
            ];

            self::send(json_encode($params));
        }
    }

    /**
     * Send message
     *
     * @param $message
     */
    private static function send($message)
    {
        $socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);

        socket_sendto($socket, $message, strlen($message), 0, self::$server['host'], self::$server['port']);

        socket_close($socket);
    }

}
