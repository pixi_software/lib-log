<?php

namespace Pixi\Log;

use Pixi\Log\LogLevel;

class StandardDBLogger extends AbstractLogger
{
    public static $uniqId = false;

    public static $table =
        "CREATE TABLE IF NOT EXISTS `standard_log` (
          `id_log` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
          `level` enum('emergency','alert','critical','error','warning','notice','info','debug') COLLATE utf8_unicode_ci NOT NULL,
          `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
          `context` text COLLATE utf8_unicode_ci,
          `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
          `type` varchar(32) COLLATE utf8_unicode_ci NULL,
          `request_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
          `source` varchar(32) COLLATE utf8_unicode_ci NULL,
          PRIMARY KEY (`id_log`),
          INDEX (`date_create`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

    public static $adapter;

    /**
     * @param string $level debug, info, notice, warning, error, critical, alert, emergency
     * @param string $message
     * @param array $context
     * @param string $type
     * @param string $source
     * @param boolean $jsonContext
     */
    public function log($level, $message, $context = array(), $type = null, $source = null, $jsonContext = true)
    {

        if(LogLevel::$logLevel[$level] >= self::$logLevel) {

            $data = array(
                'level'         => $level,
                'message'       => $message,
                'type'          => $type,
                'request_id'    => self::getRequestNum(),
                'source'        => $source
            );

            if($jsonContext) {
                $data['context'] = json_encode($context);
            } else {
                $data['context'] = $context;
            }

            self::$adapter->insert('standard_log', $data);

        }

    }

    public static function getRequestNum()
    {
        if(!static::$uniqId) {
            self::setRequestNum(date('Y-m-d H:i:s'));
        }

        return static::$uniqId;
    }

    public static function setRequestNum($uniqId)
    {
        self::$uniqId = $uniqId;
    }

}