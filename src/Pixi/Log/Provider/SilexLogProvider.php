<?php

namespace Pixi\Log\Provider;

use Pimple\ServiceProviderInterface;
use Pimple\Container;
use Pixi\Log\UdpSocketLogger;
use Pixi\Log\StandardDBLogger;

class SilexLogProvider implements ServiceProviderInterface
{

    public function register(Container $app)
    {

        if (isset($app['pixi.config']['cfg']['pixiLogger']['UdpSocketLogger'])) {
            $handler = UdpSocketLogger::init($app['pixi.config']['cfg']['pixiLogger']['UdpSocketLogger']);
        } else {
            $handler = new StandardDBLogger;
            $handler::$adapter = $app['dbs']['customerdb'];
        }

        if (isset($app['pixi.config']['cfg']['pixiLogger']['logLevel'])) {
            $handler::$logLevel = $app['pixi.config']['cfg']['pixiLogger']['logLevel'];
        }

        $app['log'] = function () use ($handler) {
            return $handler::getLogger();
        };

    }

}
