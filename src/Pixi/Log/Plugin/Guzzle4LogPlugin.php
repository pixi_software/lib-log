<?php

namespace Pixi\Log\Plugin;

use GuzzleHttp\Event\EventEmitterInterface;
use GuzzleHttp\Event\SubscriberInterface;

use GuzzleHttp\Event\ErrorEvent;
use GuzzleHttp\Event\CompleteEvent;

class Guzzle4LogPlugin implements SubscriberInterface
{
    
    public $logger;
    public $bodyIsXml = false;
    
    public function __construct()
    {
        $this->logger = \Pixi\Log\StandardDBLogger::getLogger();
    }
    
    public function getEvents()
    {
        
        return array(
            'complete'  => array('onRequestComplete'),
            'error'     => array('onRequestError')
        );
        
    }
    
    public function onRequestComplete(CompleteEvent $event, $name)
    {
                
        $this->log(\Pixi\Log\LogLevel::INFO, __FUNCTION__, $event);
        
    }
    
    public function onRequestError(ErrorEvent $event, $name)
    {
    
        $this->log(\Pixi\Log\LogLevel::ERROR, __FUNCTION__, $event);
    
    }
    
    public function log($level, $function, $payload)
    {     
        
        $arr = array();
        
        $arr['request.url'] = $payload->getRequest()->getUrl();
        
        $arr['request.param'] = $payload->getRequest()->getQuery()->toArray();
                
        if($body = (string) $payload->getResponse()->getBody()) {
            
            if($this->bodyIsXml) {
                $arr['response.body'] = htmlentities($body);
            } else {
                $arr['response.body'] = $body;
            }
            
        }
  
        $this->logger->log($level, $function, $arr, 'request', 'http.client');
        
    }
    
}
