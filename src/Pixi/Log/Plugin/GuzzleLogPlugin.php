<?php

namespace Pixi\Log\Plugin;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class GuzzleLogPlugin implements EventSubscriberInterface
{
    
    public $logger;
    
    public function __construct()
    {
        $this->logger = \Pixi\Log\StandardDBLogger::getLogger();
    }
    
    public static function getSubscribedEvents()
    {
        
        return array(
            'request.complete'  => array('onRequestComplete', 501),
            'request.error'     => array('onRequestError', 502),
            'request.exception' => array('onRequestException', 503)
        );
        
    }
    
    public function onRequestComplete($payload)
    {
        
        $this->log(\Pixi\Log\LogLevel::INFO, __FUNCTION__,$payload);
        
    }
    
    public function onRequestError($payload)
    {
    
        $this->log(\Pixi\Log\LogLevel::ERROR, __FUNCTION__, $payload);
    
    }
    
    public function onRequestException($payload)
    {
    
        $this->log(\Pixi\Log\LogLevel::CRITICAL, __FUNCTION__, $payload);
    
    }
    
    public function log($level, $function, $payload)
    {
        
        $arr = array();
        
        $arr['request.url'] = $payload['request']->getUrl();
        
        if($payload['request'] instanceof \Guzzle\Http\Message\EntityEnclosingRequestInterface) {
            $arr['request.param'] = $payload['request']->getPostFields()->__toString();
        }
        
        if($body = (string) $payload['response']->getBody()) {
            $arr['response.body'] = $body;
        }
        
        $this->logger->log($level, $function, $arr, 'request', 'http.client');
        
    }
    
}
