Pixi* log Library
=======================

Pixi* log Library is used for logging entries based on monolog/PSR-3 standard.
Logging to database and UDP socket is supported.

# Examples

Using SDK 1.x with *StandardDBLogger*:

```php
use Pixi\Log\StandardDBLogger;

$ci = get_instance();

$logHandler = new StandardDBLogger();
$logHandler::$adapter = $ci->customerdb;
$logHandler::$logLevel = 100;

$logger = $logHandler::getLogger();
$logger$app['log'];
```

Using SDK 1.x with *UdpSocketLogger* and *\Pixi\AppsFactory\Environment*:

```php
use Pixi\Log\UdpSocketLogger;

$logHandler = UdpSocketLogger::init([
    'host' => 'example.net',
    'port' => 1234
]);
$logHandler::setConfig(
    \Pixi\AppsFactory\Environment::getEnvironment(),
    \Pixi\AppsFactory\Environment::getCustomer(),
    'sdk-1.x-app-alias',
    'app-version',
    \Pixi\AppsFactory\Environment::getUser()
);

$logger = $logHandler::getLogger();
$logger->log($level, $message, $context, $method, $class);
```

Using SDK 2.x with *StandardDBLogger*:

```php
$app = new Silex\Application();
$app->register(new \Pixi\Log\Provider\SilexLogProvider());
$app['log']->log($level, $message, $context, $type, $source);
```

Using SDK 2.x with *UdpSocketLogger* and *\Pixi\AppsFactory\Environment* you have to set server and host in *pixi.php* config file:

```php
$cfg['pixiLogger']['UdpSocketLogger'] = [
    'host' => 'example.net',
    'port' => 1234    
];

$app = new Silex\Application();
$app->register(new \Pixi\Log\Provider\SilexLogProvider());
$app['log']::setConfig(
    \Pixi\AppsFactory\Environment::getEnvironment(),
    \Pixi\AppsFactory\Environment::getCustomer(),
    'sdk-2.x-app-alias',
    'app-version',
    \Pixi\AppsFactory\Environment::getUser()
);

$app['log']->log($level, $message, $context, $method, $class);
```
