<?php

namespace Pixi\Log;

use Pixi\Log\LogLevel;

abstract class AbstractLogger
{
    /**
     * @var AbstractLogger
     */
    public static $logger;

    /**
     * @var int default log level
     */
    public static $logLevel = 100;

    public abstract function log($level, $message, $context = array(), $method = null, $class = null, $jsonDetails = true);

    /**
     * @return AbstractLogger
     */
    public static function getLogger()
    {
        if(is_null(self::$logger)) {
            $self = get_called_class();
            self::$logger = new $self();
        }

        return self::$logger;
    }

    public static function setLogLevel($level)
    {
        $logLevel = LogLevel::$logLevel;

        self::$logLevel = $logLevel[$level];
    }
}
